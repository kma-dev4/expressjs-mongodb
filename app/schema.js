// load Joi module
const Joi = require('joi');

// Registration User Schema
const registerDataSchema = Joi.object({
    first_name: Joi.string().required().label('First Name'),
    last_name: Joi.string().required().label('Last Name'),
    mobile_number: Joi.string().length(10).required().label('Mobile Number'),
    email: Joi.string().required().email().label('Email Address'),
    password: Joi.string().required().min(6).label('Password')
});

// Authantication Login Schema
const authDataSchema = Joi.object({
    email: Joi.string().email().lowercase().required().label('Email Address'),
    password: Joi.string().min(6).required().label('Password')
});

/**
 * Export the schemas
 *
 * @key Route URL
 * @value Route Schema
 */
module.exports = {
    '/auth/login': authDataSchema,
    '/auth/register': registerDataSchema,
};
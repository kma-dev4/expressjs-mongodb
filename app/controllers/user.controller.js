/***
 * User Controller
 */

// load required modules
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var path = require('path');
var bcrypt = require('bcryptjs');
var nodeMailer = require('nodemailer');
var EmailTemplate = require('email-templates').EmailTemplate;

// load user model
var User = require('../models/user.model');

// load required configuration
var mail = require('../../config/mail');
var config = require('../../config/app');

// New User Registration
exports.register = (req, res) => {

	body = req.body;

    user = new User({
		first_name: body.first_name,
		last_name: body.last_name,
		mobile_number: body.mobile_number,
		email: body.email,
		password: body.password
	});

	user.save(function(err, user) {
		if (err) return res.fail('Email address already register with us.');

		// Sending Email to register email address
		let transporter = nodeMailer.createTransport(mail);
		let send = transporter.templateSender(
			new EmailTemplate(path.join(__dirname, '..', '/templates/register')),
			{ from: 'ExpressJS - MongoDB <ketan.indglobal@gmail.com>' }
		);

		send({
			to: user.email
		}, {
			name: user.name,
			email: user.email,
			token: user.id
		}, function (err, info) {
			if (err) return res.fail(err.message)
			let token = jwt.sign({ id: user._id }, config.key, { expiresIn: 86400 });
			res.success("Register Successfully!...", { token: token, 'type': 'Bearer' });
		});

	});

};


// Login
exports.login = (req, res) => {

	User.getAuthenticated(req.body.email, req.body.password, function(err, user, reason) {
		// Error in authentication
		if(err) {
			res.fail(err.message)
		}

		// Email and password correct
		if(user) {
			var token = jwt.sign({ id: user._id }, config.key, { expiresIn: 86400 });
			res.success("Login Successfully!...", { token: token, 'type': 'Bearer' });
		}

		var reasons = User.failedLogin;
        switch (reason) {
            case reasons.NOT_FOUND:
            	res.fail('Authentication failed.', ['User not found.'])
                break;
            case reasons.PASSWORD_INCORRECT:
            	res.fail('Authentication failed.', ['Wrong password.'])
                break;
            case reasons.MAX_ATTEMPTS:
            	res.fail('Authentication failed.', ['Max attempts.'])
                break;
        }
	});

};

// Auth User
exports.user = (req, res, next) => {

	res.success('Yahhh!..', ['We got it!.'])
};
var cors = require('cors')
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var passport = require('passport');
var logger = require('morgan');
var morgan = require('morgan');

// Configuring the database
var config = require('./config/app');
var mongoose = require('mongoose');

var multer = require('multer');
var upload = multer();

// create express app
var app = express();

// view engine setup
app.set('view engine', 'ejs');

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));

// Passport
app.use(morgan('dev'));
app.use(passport.initialize());

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(config.database)
.then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

// Apply core middleware
app.use(cors())

// Response Macros
app.use(function(req, res, next) {
	res.success = function(message, data = [], status = 200) {
		return res.status(status).json({'status':true, 'message': message, 'data': data})
	}
	res.fail = function(message, data = [], status = 422) {
		return res.status(status).json({'status':false, 'message': message, 'errors': data})
	}
	next();
})

// Routes
require('./app/routes/auth.routes.js')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Route Not Found');
  err.status = 404;
  res.fail(err.message)
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).json({'message': err.message});
});

// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});
